var gulp = require('gulp');
var less = require('gulp-less');
var clean = require('gulp-clean');


var app = {
 src: './source',
 pub: './public',
 vnd: './vendor'
};

gulp.task('clean', function() {
 return gulp.src(app.pub)
 .pipe(clean());
});

gulp.task('compileless', function() {
    console.log('Starting CSS Processing');
    return gulp.src(app.src + '/less/styles.less')
    .pipe(less())
    .pipe(gulp.dest(app.pub + '/css'))
});

gulp.task('copyvendorcss', function() {
    
    var src = [
        app.vnd + '/gritter/css/jquery.gritter.css'
    ];
    // Return your stream.
    return gulp.src(src)
       .pipe(gulp.dest(app.pub + '/css'));
   
});

gulp.task('copyfonts', function() {
    
    var src = [
        app.vnd + '/bootstrap/fonts/*.*',
        app.vnd + '/fontawesome/fonts/*.*'
    ];
    // Return your stream.
    return gulp.src(src)
       .pipe(gulp.dest(app.pub + '/fonts'));
   
});

gulp.task('copyjs', function() {
    
    var src = [
        app.vnd + '/angular/angular.min.js',
        app.vnd + '/angular/angular.min.js.map',
        app.vnd + '/angular-animate/angular-animate.min.js',
        app.vnd + '/angular-animate/angular-animate.min.js.map',
        app.vnd + '/angular-route/angular-route.min.js',
        app.vnd + '/angular-route/angular-route.min.js.map',
        app.vnd + '/bootstrap/dist/js/bootstrap.min.js',
        app.vnd + '/jquery/dist/jquery.min.js',
        app.vnd + '/gritter/js/jquery.gritter.min.js',
        app.src + '/js/*.js'
    ];
    // Return your stream.
    return gulp.src(src)
       .pipe(gulp.dest(app.pub + '/js'));
   
});

gulp.task('copyhtml', function() {
   var src = [
        app.src + '/**/*.html',
        app.src + '/**/*.htm'
    ];
   return gulp.src(src)
   .pipe(gulp.dest(app.pub));
});

gulp.task('copyimages', function() {
    var src = [
        app.vnd + '/gritter/images/*.*',
        app.src + '/images/**/*.*'
    ];
   return gulp.src(src)
   .pipe(gulp.dest(app.pub + '/images'));
});

gulp.task('copymisc', function() {
   return gulp.src(app.src + '/favicon.ico')
   .pipe(gulp.dest(app.pub));
});


gulp.task('default', ['clean'], function(){
   gulp.start(['compileless','copyvendorcss','copyfonts','copyhtml','copyjs','copyimages','copymisc']);   
});