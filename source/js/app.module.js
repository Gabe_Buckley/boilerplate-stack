// Define the `website` module
var website = angular.module('website', ['ngRoute','ngAnimate']);

website.controller('WebsiteController', function WebsiteController($scope) {
  window.MY_SCOPE = $scope;
  $scope.siteTitle = "gabrielbuckley.com";
});

websiteApp = angular.
  module('website').
  config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');

      $routeProvider
        .when("/", {
            title: 'gabrielbuckley.com - Home',
            templateUrl : "templates/main.htm",
            controller: 'mainController'
        })
        .when("/about", {
            title: 'gabrielbuckley.com - About',
            templateUrl : "templates/about.htm",
            controller: 'aboutController'
        })
        .when("/stack", {
            title: 'gabrielbuckley.com - Stack',
            templateUrl : "templates/stack.htm",
            controller: 'stackController'
        })
        .when("/resume", {
            title: 'gabrielbuckley.com - Resume',
            templateUrl : "templates/resume.htm",
            controller: 'resumeController'
        })
        .when("/stuff", {
            title: 'gabrielbuckley.com - Stuff',
            templateUrl : "templates/stuff.htm",
            controller: 'stuffController'
        })
        .when("/contact", {
            title: 'gabrielbuckley.com - Contact',
            templateUrl : "templates/contact.htm",
            controller: 'contactController'
        });
    }
  ]);

websiteApp.run(['$rootScope', function($rootScope) {
    $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
        $rootScope.title = current.$$route.title;
    });
}]);

websiteApp.controller('mainController', function($scope) {
    $scope.pageClass = 'page-home';
});

websiteApp.controller('aboutController', function($scope) {
    $scope.pageClass = 'page-about';
});

websiteApp.controller('stackController', function($scope) {
    $scope.pageClass = 'page-stack';
    
    $scope.bubbler = {
        on_btn_class: 'btn-success',
        off_btn_class: 'btn-default',
        wrap_class: '',
        toggle: function(){
            if($scope.bubbler.wrap_class != ''){
                $scope.bubbler.on();
            } else {
                $scope.bubbler.off();
            }
        },
        on: function(){
            $scope.bubbler.wrap_class = '';
            $scope.bubbler.on_btn_class = 'btn-success';
            $scope.bubbler.off_btn_class = 'btn-default';
            $.gritter.add({
				// (string | mandatory) the heading of the notification
				title: 'Weeeeeeeee Bubbbles!',
				// (string | mandatory) the text inside the notification
				text: '',
				// (bool | optional) if you want it to fade out on its own or just sit there
				sticky: false,
				// (int | optional) the time you want it to be alive for before fading out
				time: '15',
                position: 'top-right'
			});
            
        },
        off: function(){
            $scope.bubbler.wrap_class = 'hide';
            $scope.bubbler.on_btn_class = 'btn-default';
            $scope.bubbler.off_btn_class = 'btn-danger';
            $.gritter.add({
				// (string | mandatory) the heading of the notification
				title: 'Bubbles have been switched off',
				// (string | mandatory) the text inside the notification
				text: '',
				// (bool | optional) if you want it to fade out on its own or just sit there
				sticky: false,
				// (int | optional) the time you want it to be alive for before fading out
				time: '15',
                position: 'top-right'
			});

        }
    };
});

websiteApp.controller('resumeController', function($scope) {
    $scope.pageClass = 'page-resume';
    $scope.workHistory = [
        {
            "title":  "Consultant - SMS Management & Technology",
            "description": [
                "SMS is a leader in advisory, solutions and managed services. My roles include business analysis and software developmen across a range of industry sectors including construction and government."
            ],
            "date": "Jun 2016 - Current"
        },
        {
            "title":  "Senior Software Developer - Dealer Trade",
            "description": [
                "Leading a team of 4 developers creating a responsive web and mobile app to facilitate the sale of second-hand vehicles among used car dealers. Using node.js and meteor.js connected to NoSQL MongoDB as data store with Google Material, Framework 7 and JQuery on the front end. My role as Senior Software Developer incorporates project management and scrum-master duties."
            ],
            "date": "Mar 2016 - Jun 2016"
        },
        {
            "title":  "Software Developer - AAMC",
            "description": [
                "Worked in a team to analyse, scope and rebuild a complex automotive insurance assessment and management application. Developing with a backend of CodeIgniter (PHP) with MySQL and a HTML5 / Bootstrap / JQuery front end implemented with the Twig templating system. My role incorporated the integration with numerous external services as well as developing aspects of the software across the full stack of technologies."
            ],
            "date": "2015 - 2016"
        },
        {
            "title":  "Senior Developer - Zippy (BDS)",
            "description": [
                "Developed and maintained software and web sites related to the Zippy smartphone app. Zippy is a brand loyalty and marketing solution aimed at small to medium retailers. My role included taking over from an off-shore development team and bringing development efforts in-house, developing new features, planning ongoing development activities and managing junior staff. Technologies included PHP including MVC frameworks, HTML/CSS/JS including jQuery and Bootstrap."
            ],
            "date": "2014 - 2015"
        },
        {
            "title":  "Web Development Manager - Divest IT",
            "description": [
                "Divest IT is a cloud computing provider developing complex cloud-based web applications for the corporate market. My role has been to create and develop a web development team within Divest IT and develop processes around web development. I have also been involved in hands-on development of both front-end and back-end web applications across a number of technologies."
            ],
            "date": "2013 - 2014"
        },
        {
            "title":  "Web Technology Manager - 6YS Cloud Computing Specialists",
            "description": [
                "6YS is a leading Australian cloud computing services provider. My role included managing and taking active roles in the development and maintenance of client and internal web sites and applications across a range of technologies in a cloud computing environment."
            ],
            "date": "2012 - 2013"
        },
        {
            "title":  "Web Developer - Guvera",
            "description": [
                "Guvera is an online music streaming service. My role has been to redevelop the interface of the web site including the media player and playlist management interfaces. Working in a team of designers and developers using Java, JSP, Javascript, CSS and HTML. "
            ],
            "date": "2012 - 2012"
        },
        {
            "title":  "Contract Development Team Lead - Mooball IT",
            "description": [
                "Mooball IT is a digital agency with specialist skills in management, technical and creative aspects of digital technology. Specialising in design and development of web-based applications and enterprise level content management systems. I worked as a project manager across multiple projects managing a team of 4 developers."
            ],
            "date": "2011 - 2012"
        },
        {
            "title":  "Senior Web Developer - MCS Australia",
            "description": [
                "MCS is a leading consulting services organisation servicing the mining sector. ",
                "I developed a suite of web-based mining efficiency applications leveraging real-time sensor data and the use of intrinsically safe PDA devices for production usage in underground mining environments. The development environment was C#, VB.Net, custom AJAX libraries, HTML, CSS and mobile computing platforms."
            ],
            "date": "2010 - 2011"
        },
        {
            "title":  "Senior Web Developer - Digital Instinct Pty Ltd",
            "description": [
                "Developing rich web client applications and complementary web services to provide a web interface to a suite of roster-creation and time-and-attendance software applications. I created a dynamic client-side JavaScript library to facilitate the deployment of highly interactive web applications. These client applications communicate through a custom AJAX layer with SOAP-based web services I have written using C# on the .NET platform."
            ],
            "date": "2009 - 2010"
        },
        {
            "title":  "Development Manager - IServices Consulting Pty Ltd",
            "description": [
                "Managed and mentored a team of business analysts and software developers delivering custom solutions using C# and based on the .NET platform using the Umbraco content management system, SharePoint, Web Parts and Google toolkits. I managed a portfolio of projects catering predominantly to the not-for-profit sector performing business analysis, consulting, project management and development activities as required."
            ],
            "date": "2009"
        },
        {
            "title":  "Lead Developer - Puttoo Limited",
            "description": [
                "Scoped, designed and developed custom web software including start-up web venture “Puttoo”. Puttoo was created using a completely open-source approach, built on the eXist XML server engine it leveraged the XPath, XQuery and XForms technology together with a custom AJAX layer to create a web forms engine with a rich web client front-end. I was the co-founder and senior technical resource from the inception of the project to its delivery to the marketplace."
            ],
            "date": "2006 - 2009"
        },
        {
            "title":  "Senior Consultant Developer - Eos Solutions",
            "description": [
                "Analysed and developed bespoke and shrink-wrapped software products based primarily on the IBM technologies Lotus Domino and Websphere. ",
                "Combined back-end software development skills in these technologies (LotusScript and Java) with front-end web development skills in CSS and JavaScript to produce rich, interactive solutions delivered through the web. Of particular importance was the development of a custom workflow and forms engine deployed across four countries."
            ],
            "date": "2001 - 2006"
        },
        {
            "title":  "Senior Consultant Developer - Eos Solutions",
            "description": [
                "Implemented a comprehensive consulting and development methodology around our Domino-based web content management system Rapid Web Publisher. Acted as senior consultant in deploying Rapid Web Publisher to a number of clients across the manufacturing, education and government sectors. Rapid Web Publisher was built on Lotus Domino and utilised client-side technologies such as HTML, JavaScript and CSS as well as back-end Java and LotusScript development work."
            ],
            "date": "1999 - 2001"
        }
    ];
});

websiteApp.controller('stuffController', function($scope) {
    $scope.pageClass = 'page-stuff';
});

websiteApp.controller('contactController', function($scope) {
    $scope.pageClass = 'page-contact';
    $scope.pageTitle = 'Contact';
    $scope.contactOptions = [
        {
            "name":     "Bitbucket",
            "url":      "https://bitbucket.org/Gabe_Buckley",
            "handle":   "Gabe_Buckley",
            "icon":     "fa-bitbucket"
        },
        {
            "name":     "Slack",
            "url":      "https://slack.com/",
            "handle":   "@gabe_buckley",
            "icon":     "fa-slack"
        },
        {
            "name":     "Pinterest",
            "url":      "https://au.pinterest.com/gabebuckley/",
            "handle":   "gabebuckley",
            "icon":     "fa-pinterest-p"
        },
        {
            "name":     "Twitter",
            "url":      "https://twitter.com/",
            "handle":   "@gabrielbuckley",
            "icon":     "fa-twitter"
        },
        {
            "name":     "Facebook",
            "url":      "https://www.facebook.com/gbuckley",
            "handle":   "Gabriel J. Buckley",
            "icon":     "fa-facebook"
        },
        {
            "name":     "Email",
            "url":      "mailto:gb@gabrielbuckley.com",
            "handle":   "gb@gabrielbuckley.com",
            "icon":     "fa-envelope-o"
        }];
});
